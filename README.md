## Postman collection to check servers matained by Coopdevs

### Requirements

Install [Newman](https://github.com/postmanlabs/newman) (CLI) or [Postman](https://getpostman.com/) (GUI) and clone this repo.

We are using Newman for this README

### Test them all

`newman run health_check.postman_collection.json`

### Test Coopdevs own servers

`newman run --folder "Coopdevs" health_check.postman_collection.json`

### Test Coopdevs servers for Som Connexió

`newman run --folder "Som Connexió" health_check.postman_collection.json`
